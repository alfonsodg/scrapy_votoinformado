from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector
from jne.items import JneItem
#from craigslist_sample.items import CraigslistSampleItem
#from scrapy.selector import Selector
#from urlparse import urlparse
#from farm1.items import Farm1Item
#from scrapy.linkextractors import LinkExtractor

class DmozSpider(CrawlSpider):
    name = "jne"
    allowed_domains = ["www.votoinformado.pe"]
    start_urls = [
        "http://www.votoinformado.pe/voto/congresistas_partidos.aspx?o=V7ioQ6R7RhVA1JKjYPi5bg==",
    ]
    #rules = (
    #    Rule(SgmlLinkExtractor(allow=(), restrict_xpaths=('*//a/@href',)), callback="parse_items", follow= True),
    #)
    rules = (Rule(SgmlLinkExtractor(allow=("congreso-candidatos" )), callback="parse_items", follow= True),)

    def parse_items(self, response):
        hxs = HtmlXPathSelector(response)
        all_links = hxs.xpath('//a[contains(@href, "hoja")]/@href').extract()
        items = []
        for link in all_links:
            item = JneItem()
            item['url'] = link
            items.append(item)
        return items
